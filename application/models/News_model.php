<?php

    class News_model extends CI_Model
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->database();
        }

        public function get_news( $slug = NULL )
        {
            if ( $slug === NULL )
            {
                $query = $this->db->get( 'news' );
                return $query->result_array();
            }

            $query = $this->db->get_where( 'news', array( 'slug' => $slug ) );
            return $query->row_array();
        }

        public function set_news()
        {
            $this->load->helper( 'url' );

            $slug = url_title( $this->input->post( 'title' ), 'dash', TRUE );

            $data = array(
                'title' => $this->input->post( 'title' ),
                'slug' => $slug,
                'text' => $this->input->post( 'text' )
            );

            return $this->db->insert( 'news', $data );
        }

        public function edit_news()
        {
            $this->load->helper( 'url' );

            $id = $this->input->post('id');

            $slug = url_title( $this->input->post( 'title' ), 'dash', TRUE );

            $data = array(
                'title' => $this->input->post( 'title' ),
                'slug' => $slug,
                'text' => $this->input->post( 'text' )
            );

            return $this->db->update( 'news', $data, array( 'id' => $id) );
        }

        public function delete_news( $id )
        {
            return $this->db->delete( 'news', array( 'id' => $id ) );
        }
    }
